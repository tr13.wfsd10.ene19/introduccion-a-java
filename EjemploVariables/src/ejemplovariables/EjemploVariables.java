
package ejemplovariables;


public class EjemploVariables {

   
    public static void main(String[] args) {
        
        //boolean
        
        boolean valorLogico = false;
        System.out.println("el valor es " + valorLogico);
        //caracteres
        char valorCaracter ='f';
        System.out.println("el valor de la variable es " + valorCaracter);
        //strings 
        
        String cadena = "soy una cadena";
        System.out.println(cadena);
        //enteros
        byte valorUno = 10;
        short valorDos = 20;
        int valorTres = 30;
        long valorCuatro = 40;
        System.out.println("el numero uno es " + valorUno);
        //tipos de datos reales 
        
        //float
        
        float numeroCinco = 4.6f;
        double numeroSeis = 45.7;
        
        //constantes
        
        final int VALOR = 40;
        
        
        
        
        
    }
    
}
